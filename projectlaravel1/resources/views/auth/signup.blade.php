@extends('layout.master')
@section('judul')
Sign Up
@endsection
@section('content')
    <h1>Buat Acoount Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/send" method="post">
        @csrf
        <label for="">First Name:</label> <br>
        <input type="text" name="first_name">
        <br><br>
        <label for="">Last Name:</label><br>
        <input type="text" name="last_name">
        <br><br>
        <label for="">Gender:</label> <br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female
        <br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
        </select>
        <br><br>
        <label for="language">Language Spoken :</label><br>
        <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="English">English <br>
        <input type="checkbox" name="language" value="Arabic">Arabic <br>
        <input type="checkbox" name="language" value="Japanese">Japanese 
        <br><br>
        <label for="bio">Bio :</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br><br>
        <button type="submit">Sign Up</button>

        @endsection