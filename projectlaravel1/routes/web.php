<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/table', function () {
    return view('auth.table');
});
Route::get('/data-tables', function () {
    return view('auth.data-tables');
});

Route::get('/', 'HomeController@index');
Route::get('/signup', 'AuthController@signup');
Route::post('/send', 'AuthController@welcome');


// CRUD cast

// Create Data
Route::get('/cast/create', 'CastController@create');
// Save data to database
Route::post('/cast', 'CastController@store');
// Read Data
Route::get('/cast', 'CastController@index');
// Read Data By ID
Route::get('/cast/{id}', 'CastController@show');
// Go to edit page
Route::get('/cast/{id}/edit', 'CastController@edit');
// Update Data Cast
Route::put('/cast/{id}', 'CastController@update');
// Delete Data Cast
Route::delete('/cast/{id}', 'CastController@destroy');