<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup()
    {
        return view('auth.signup');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');

        return view('auth.welcome', compact('first_name', 'last_name'));
    }
}
